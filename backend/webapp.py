#!/usr/bin/env python3
# Entry point for the application.
from w_app import app    # For application discovery by the 'flask' command.
from w_app import views  # For import side-effects of setting up routes.
