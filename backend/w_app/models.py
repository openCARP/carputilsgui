#!/usr/bin/env python3
from . import app, db
from sqlalchemy.dialects.postgresql import ARRAY
from flask_marshmallow import Marshmallow
from marshmallow_sqlalchemy import ModelSchema, field_for


#Classes for save data automatically in database

ma = Marshmallow(app)

class ArgumentDB(db.Model):
    __tablename__ = 'Arguments'
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(120), unique = False)
    default = db.Column(db.String(120), unique = False)
    required = db.Column(db.String(120), unique = False)
    #type = db.Column(db.String(120), unique = False)
    choices = db.Column(ARRAY(db.String()), unique = False)
    help = db.Column(db.String(320), unique = False)
    value = db.Column(db.String(120), unique = False)
    action_store = db.Column(db.String(120), unique = False) 
    subtut_n = db.Column(db.String, db.ForeignKey('Subtutorials.name'))

    #def __init__(self, name, default, required, type, choices, help, value, subtut_n):
    def __init__(self, name, default, required, choices, help, value, action_store, subtut_n):
        self.name = name
        self.default = default
        self.required = required
        #self.type = type
        self.choices = choices
        self.help = help
        self.value = value
        self.action_store = action_store
        self.subtut_n = subtut_n


class SubtutorialDB(db.Model):
    __tablename__ = 'Subtutorials'
    name = db.Column(db.String(120), primary_key=True)
    path = db.Column(db.String(620), unique = False)
    descript = db.Column(db.String(620), unique = False)
    author = db.Column(db.String(620), unique = False)
    doc = db.Column(db.String(), unique = False)
    tut_n = db.Column(db.String, db.ForeignKey('Tutorials.name'))
    argument = db.relationship('ArgumentDB', backref='arg', lazy='dynamic')
    
    def __init__(self, name, path, descript, author, doc, tut_n):
        self.name = name
        self.path = path
        self.descript = descript
        self.doc = doc
        self.author = author
        self.tut_n = tut_n


class TutorialDB(db.Model):
    __tablename__ = 'Tutorials'
    name = db.Column(db.String(120), primary_key=True)
    subtut = db.relationship('SubtutorialDB', backref='tuto', lazy='dynamic')
    
    def __init__(self, name):
        self.name = name


class ArgumentDBSchema(ma.ModelSchema):
    #id = field_for(ArgumentDB, 'id', dump_only=True)

    class Meta:
        model = ArgumentDB


arg_schema = ArgumentDBSchema()
args_schema = ArgumentDBSchema(many=True)

class SubtutorialDBSchema(ma.ModelSchema):
    class Meta:
        model = SubtutorialDB


subtut_schema = SubtutorialDBSchema()
subtuts_schema = SubtutorialDBSchema(many=True)


class TutorialDBSchema(ma.ModelSchema):
    class Meta:
        model = TutorialDB


tut_schema = TutorialDBSchema()
tuts_schema = TutorialDBSchema(many=True)






