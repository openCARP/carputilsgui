#!/usr/bin/env python3
from . import db
from . import models
from . import getrunpy
import os
import sys
sys.path.append('/')
import tutorials

def create():
    db.drop_all()
    db.create_all()

    #path = os.path.dirname(tutorials.__file__)
    path = '/tutorials'
    dic_tutorials = getrunpy.get_runPaths(path)


    #Fill DB with all data

    #Tutorials Table
    for tut in dic_tutorials:
        tutorial= models.TutorialDB(tut)
        print ('PATH: '+ path + '  Tut:' + tut)
        db.session.add(tutorial)
        db.session.commit()

        #Subtutorials Table
        for subtut in dic_tutorials[tut]:
            pathrun = 'tutorials.'+subtut[1]+'.run'
            modrun = __import__(pathrun, globals(), locals(),fromlist=['GUIinclude','EXAMPLE_DESCRIPTIVE_NAME','EXAMPLE_AUTHOR', 'parser'])
            try:
                if modrun.GUIinclude:
                    print('Subtut:' + pathrun)
                    subtutorial = models.SubtutorialDB(subtut[0], subtut[1], modrun.EXAMPLE_DESCRIPTIVE_NAME,
                                                       modrun.EXAMPLE_AUTHOR,
                                                       modrun.__doc__, tutorial.name)
                    db.session.add(subtutorial)
                    db.session.commit()
                    parse = modrun.parser()
                    options = parse._actions

                    # Arguments Table
                    for k in options:
                        argName = ''
                        argNamelist = getattr(k, 'option_strings')
                        argName += argNamelist[0]
                        argName = argName[2:]
                        if (str(getattr(k, 'default')) == 'false') or (str(getattr(k, 'default')) == 'False') or (
                                str(getattr(k, 'default')) == 'true') or (str(getattr(k, 'default')) == 'True'):
                            argument = models.ArgumentDB(argName, getattr(k, 'default'), getattr(k, 'required'),
                                                         ['false', 'true'], getattr(k, 'help'), getattr(k, 'default'),
                                                         str(getattr(k, 'const')), subtutorial.name)
                        else:
                            argument = models.ArgumentDB(argName, getattr(k, 'default'), getattr(k, 'required'),
                                                         getattr(k, 'choices'), getattr(k, 'help'),
                                                         getattr(k, 'default'), str(getattr(k, 'const')),
                                                         subtutorial.name)

                        db.session.add(argument)
                        db.session.commit()
                    # Add ID parameter for experiment folder name (Required in GUI)
                    argument = models.ArgumentDB('ID', 'exp', 'false', None,
                                                 "Manually specify the job ID (output directory) (no blanks)", 'exp',
                                                 None, subtutorial.name)
                    db.session.add(argument)
                    db.session.commit()
            except AttributeError:
                pass


