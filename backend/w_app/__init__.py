#!/usr/bin/env python3
from . import config
import os
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import exc
import time

db = SQLAlchemy()


while 1:
    try:
        app = Flask(__name__)
        app.config["SQLALCHEMY_DATABASE_URI"] = config.DATABASE_CONNECTION_URI
        app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

        db = SQLAlchemy(app)
        db.init_app(app)
        db.drop_all()
    except exc.OperationalError:
        print('Waiting for database...')
        time.sleep(1)
    else:
        break

print('Connected!')