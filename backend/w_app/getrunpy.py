#!/usr/bin/env python3
import os


def get_runPaths(src_path):

    dic_tutorials = {}
    #Tutorials
    for nom_tutorial in os.listdir(src_path):
        if os.path.isdir(os.path.join(src_path, nom_tutorial)):
            if ('pycache' not in nom_tutorial) and ('visualization' not in nom_tutorial):
                dic_tutorials[nom_tutorial] = []
                pathTut = f"{src_path}/{nom_tutorial}"
                #Subtutorials
                for nom_subtutorial in os.listdir(pathTut):
                    if os.path.isdir(os.path.join(pathTut, nom_subtutorial)):
                        if 'pycache' not in nom_subtutorial:
                            dic_tutorials[nom_tutorial].append((nom_subtutorial, nom_tutorial+'.'+nom_subtutorial))

    return dic_tutorials





######################################


