#!/usr/bin/env python3
from flask import Flask, jsonify, request, session, Response, stream_with_context, json
from . import app
from . import models
from flask_cors import CORS
from flask import Response
from . import carputilsrun 
import os
from . import create_db


CORS(app)
pathBefore = os.getcwd()
jsondataExist = None

create_db.create()


@app.route('/api/tutorial/<subtutname>', methods=['GET'])
def get_all_arguments(subtutname):
    arguments = models.ArgumentDB.query.filter_by(subtut_n = subtutname).all()
    result = models.args_schema.dump(arguments)
    return jsonify(result.data)
    

@app.route('/api/tutorials', methods=['GET'])
def get_all_tuts():
    tuts = models.TutorialDB.query.order_by(models.TutorialDB.name).all()
    result = models.tuts_schema.dump(tuts)
    return jsonify(result.data)


@app.route('/api/tutorials/<tutname>', methods=['GET'])
def get_all_subtuts(tutname):
    subtuts = models.SubtutorialDB.query.filter_by(tut_n = tutname).order_by(models.SubtutorialDB.name).all()
    result = models.subtuts_schema.dump(subtuts)
    return jsonify(result.data)


@app.route('/api/subtutorials/<subtutname>', methods=['GET'])
def get_subtut(subtutname):
    subtut = models.SubtutorialDB.query.filter_by(name = subtutname).all()
    result = models.subtuts_schema.dump(subtut)
    return jsonify(result.data)


@app.route('/api/subtutorials/<subtutname>', methods=['POST'])
def executecarputils(subtutname):
    subtut = models.SubtutorialDB.query.filter_by(name = subtutname).first()
    result = request.get_json()
    resultnew = {'result':result}
    strexec = ""
    for item in resultnew['result'][39:]:
        if(item['value']):
            if '{' in (item['value']):
                item['value']=item['value'].replace('{', ' ')
                item['value']=item['value'].replace('}', ' ')
                item['value']=item['value'].replace(',', ' ')
            if str(item['action_store']) != 'None':
                if item['value'].lower() == 'true':
                    strexec += (" --" + item['name'] +" ")
            else:
                strexec += (" --" + item['name'] +" "+item['value']+" ")

    carputilsrun.run(subtut.path, strexec, pathBefore, subtutname)
    return Response(status=200)

@app.route('/api/subtutorials/read', methods=['GET'])
def readcarputils():
    return Response(stream_with_context(carputilsrun.readRun()), mimetype='text/event-stream')

@app.route('/api/subtutorials/cancel', methods=['POST'])
def cancelcarputils():
    result = request.get_json()
    carputilsrun.cancelRun()
    return jsonify(result), 201

@app.route('/api/subtutorials/jsondata', methods=['GET'])
def readjsondataTPre():
    global jsondataExist
    jsondataExist = carputilsrun.existJson()
    if jsondataExist:
        return jsonify(carputilsrun.readJsonT())
    else:
        return jsonify({'error': 'no file'}), 400

@app.route('/api/subtutorials/jsondatac', methods=['GET'])
def readjsondataCTPre():
    global jsondataExist
    jsondataExist = carputilsrun.existJson()
    jsondataExistM = carputilsrun.existJsonM()
    if not jsondataExistM and jsondataExist:
        return jsonify(carputilsrun.readJsonCT())
    else:
        return jsonify({'error': 'no file'}), 400

@app.route('/api/subtutorials/idexp', methods=['GET'])
def readIDexp():
    return jsonify(carputilsrun.readIDexp())


@app.route('/api/subtutorials/responseCarp', methods=['GET'])
def runcarpOk():
    if(carputilsrun.runcarpOk()):
        return json.dumps(True)
    else:
        return json.dumps(False)


@app.route('/api/subtutorials/novisualizeweb', methods=['GET'])
def novisualizeweb():
    if(carputilsrun.novisualizeweb()):
        return json.dumps(True)
    else:
        return json.dumps(False)
        

