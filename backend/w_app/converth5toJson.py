#!/usr/bin/env python3
import os, sys


def convert(idExp, pathBefore, epModel, path, fname, loadModule_h5):

    if loadModule_h5 is not None:
        ################TRACES#################
        filenameH5traces = path+'/'+idExp +'/'+ loadModule_h5 + "_" + "traces.h5"
        filenameJsontraces = path+'/'+idExp + "_" + "h5injsonTraces.json"

        # Create Json traces from H5 
        cmdTraces = "python /usr/src/app/w_app/h5tojson.py" + " " + filenameH5traces + " > " + filenameJsontraces
        rcT = os.system(cmdTraces)

        ################CURRENT TRACES#################
        filenameH5tracesC = path+'/'+idExp +'/'+ loadModule_h5 + "_" + "current_traces.h5"
        filenameJsontracesC = path+'/'+idExp + "_" + "h5injsonTracesC.json"
    
        # Create Json current traces from H5 
        cmdTracesC = "python /usr/src/app/w_app/h5tojson.py" + " " + filenameH5tracesC + " > " + filenameJsontracesC
        rcTC = os.system(cmdTracesC)

    else:
        ################TRACES#################
        filenameH5traces = path+'/'+idExp + "_" + epModel + "_" + "traces.h5"
        filenameJsontraces = path+'/'+idExp + "_" + "h5injsonTraces.json"

        # Create Json traces from H5 
        cmdTraces = "python /usr/src/app/w_app/h5tojson.py" + " " + filenameH5traces + " > " + filenameJsontraces
        rcT = os.system(cmdTraces)

        ################CURRENT TRACES#################
        filenameH5tracesC = path+'/'+idExp + "_" + epModel + "_" + "current_traces.h5"
        filenameJsontracesC = path+'/'+idExp + "_" + "h5injsonTracesC.json"
    
        # Create Json current traces from H5 
        cmdTracesC = "python /usr/src/app/w_app/h5tojson.py" + " " + filenameH5tracesC + " > " + filenameJsontracesC
        rcTC = os.system(cmdTracesC)


