#!/usr/bin/env python3
import json
import os
import shlex
import subprocess
import sys
from time import sleep

from . import converth5toJson

proc = None
pid = None

proc2 = None
pid2 = None
# Experiment folder name
idExp = ''
# Path to the experiment folder
pathBefore = None
path = None

epModel = None
subtutName = ''
jsondataExist = False
jsondataExistM = False
runcarpCode = False
loadModule_h5 = None
novisualize = False


def run(src_path, arguments, pathBeforeL, subtutn):
    global proc
    global pid
    global pathBefore
    pathBefore = pathBeforeL
    global idExp
    global path
    global epModel
    global subtutName
    global loadModule_h5
    subtutName = subtutn

    path = '/tutorials'
    path += '/'+ src_path.replace('.', '/')
    print ('PATH: '+path)
    os.chdir(path)
    arguments += ' --visualize --webGUI'
    loadModule_h5 = None


    #Obtain EP Model for converth5toJson (01_BASIC_BENCH)
    posEp = arguments.find('--EP')
    if posEp is not -1:
        epModel = ""
        for c in range(posEp + 5,len(arguments)):
            if arguments[c] != ' ':
                epModel += arguments[c] 
            else:
                break    
    #Obtain Load-Module for converth5toJson (04_LIMPET_FE)       
    posLm = arguments.find('--load-module')
    if posLm is not -1:
        loadModule = ""
        for c in range(posLm + 14,len(arguments)):
            if arguments[c] != ' ':
                loadModule += arguments[c] 
            else:
                break  
        loadModule_h5 = os.path.splitext(os.path.basename(loadModule))[0] 
    
    #Obtain ID experiment for converth5toJson
    posId = arguments.find('--ID')
    if posId == -1:
        arguments += (" --ID exp ")
        posId = arguments.find('--ID')

    idExp = ""
    for c in range(posId + 5,len(arguments)):
        if arguments[c] != ' ':
            idExp += arguments[c] 
        else:
            break 
          

    #Check if experiment folder exists. If true add increment number and modify --ID experiment name in arguments
    if os.path.exists(idExp) or os.path.exists('/experiments/'+subtutName+'_'+idExp):
        idExpPre = '--ID '+idExp
        i = 0
        while os.path.exists(idExp + '_' + str(i)) or os.path.exists('/experiments/'+subtutName+'_'+idExp+ '_' + str(i)):
            i += 1
        idExp += '_'+ str(i)
        idExpNew =  '--ID ' + idExp  
        arguments = arguments.replace(idExpPre, idExpNew)
        
    print('RUN: '+ arguments)
    command = f"cd {path} &&  python -u run.py {arguments}"
    proc =  subprocess.Popen([command], shell=True,stdout = subprocess.PIPE)


def readData():
    global epModel
    global idExp
    global path
    global pathBefore
    global jsondataExist
    global jsondataExistM
    global loadModule_h5
    global runcarpCode
    global novisualize
    
    jsondataExist = False
    jsondataExistM = False
    novisualize = False

    for fname in os.listdir(f"./{idExp}"):
        if (fname.endswith('.h5')):
            # Convert h5 files to Json
            converth5toJson.convert(idExp, pathBefore, epModel, path, fname, loadModule_h5)
            jsondataExist = True
            runcarpCode = True
            break

    for fname in os.listdir('.'):
        if (fname.endswith('.h5')) and (fname.startswith(idExp)):
            # Convert h5 files to Json
            converth5toJson.convert(idExp, pathBefore, epModel, path, fname, loadModule_h5)
            jsondataExist = True
            runcarpCode = True
            break
        elif (fname.endswith('.txt')) and (fname.startswith(idExp)):
            jsondataExistM = True
            jsondataExist = True
            runcarpCode = True
            break
    if not jsondataExist:
        runcarpCode = True
        novisualize = True
        
    return jsondataExist

   

def readRun():
    global proc 
    global idExp
    global jsondataExist
    global runcarpCode
    global novisualize
    folderNameExp = subtutName+'_'+idExp
    jsondataExist = False
    novisualize = False

    while True:
        line = proc.stdout.readline()
        if line == b'' and proc.poll() is not None:
            returncode = proc.returncode
            print('RETURN CODE openCARP: ',returncode)
            if os.path.exists('/experiments/'+folderNameExp):
                if (returncode == 0) and (len(os.listdir('/experiments/'+folderNameExp)) != 0):
                    runcarpCode = True
                else:
                    runcarpCode = False
            else:
                if (returncode == 0):
                    jsondataExist = readData()
                else:
                    runcarpCode = False
            del proc
            proc = None
            pid = None
            break

        line = line.strip()
        line = line.decode('utf-8')
        yield line +'\n'
    
    #Delete all experiment files and folders    
    #deleteFilesExp()

def existJson():

    return jsondataExist

def existJsonM():

    return jsondataExistM


def runcarpOk():
    return runcarpCode

def novisualizeweb():
    return novisualize


#Delete all experiment files and folders    
def deleteFilesExp():
    cmd = f"rm -rf {path}/{idExp}*"
    p = subprocess.Popen([cmd],  shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, close_fds=True)

     

def cancelRun():
    global proc 

    if proc:
        proc.kill()
        print ("Canceled Process - PID: " + str(pid))
    else:
        print ("Process don't exist")
    
    #Delete all experiment files and folders 
    deleteFilesExp()


def readJsonT():
    global pathBefore
    global idExp
    global path
    
    # Jsonfile h5 to json
    filename = path+'/'+idExp + "_" + "h5injsonTraces.json"
    # Jsonfile matplotlib to json
    filenameM = path+'/'+idExp + "_" + "matplotM.txt"
    if os.path.exists(filename):
        with open(filename) as json_file:
            dataJson = json.loads(json_file.read())
    else:
        dict3 = {} 
        dict2 = {}  
        dict1 = {}  
        fieldsExt = ['labels','datasets']
        fieldsInt = ['labelsAB','xlim','ylim','labelXY','valueX','valueY1','valueY2'] 
        with open(filenameM) as json_file:
            i = 0
            for line in json_file:
                line = line.replace("[]", "#")
                description = list(line.replace('[', '').replace(']', '').replace("'", '').replace('\n', '').split(',', 50))
                if i == 0:
                    if description[0] == '#':
                        dict2[fieldsInt[i]]= []
                    else:
                        dict2[fieldsInt[i]]= description
                    dict1[fieldsExt[0]]= dict2 
                    i = i + 1
                else:
                    if description[0] == '#':
                        dict3[fieldsInt[i]]= [] 
                    else:
                        dict3[fieldsInt[i]]= description 
                    i = i + 1
                    dict1[fieldsExt[1]]= dict3 
        out_file = open(path+'/'+"data.json", "w") 
        json.dump(dict1, out_file, indent = 4) 
        out_file.close() 

        with open("data.json") as json_file:
            dataJson = json.load(json_file)

    return dataJson

def readJsonCT():
    global pathBefore
    global idExp
    global path
    
    jsonFile = idExp + "_" + "h5injsonTracesC.json"
    filename = os.path.join(path, jsonFile)
    with open(filename) as json_file:
        dataJson = json.loads(json_file.read())
            
    return dataJson

def readIDexp():
    global idExp

    return idExp
