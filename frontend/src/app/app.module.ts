import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';
import { ScrollToBottomDirective } from './args/scroll-to-bottom.directive';

import {HttpClientModule} from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { TutorialComponent } from './tutorial/tutorial.component';
import { ArgsComponent } from './args/args.component';
import { SseService } from './args/sse.service';
import { SubtutComponent} from './subtut/subtut.component';
import {MaterialModule} from './modules/material/material.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout';
import { VisualizeComponent } from './visualize/visualize.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ResizableModule } from 'angular-resizable-element';
import { CommonModule } from '@angular/common';



@NgModule({
  declarations: [
    AppComponent,
    ArgsComponent,
    TutorialComponent,
    SubtutComponent,
    ScrollToBottomDirective,
    VisualizeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    MaterialModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    ResizableModule,
    CommonModule
  ],
  providers: [
    SseService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

