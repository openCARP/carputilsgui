import {Injectable} from '@angular/core'
import {HttpClient} from '@angular/common/http'
import {Observable} from 'rxjs'
import {Subtutorial} from './subtutorial'

@Injectable()
export class SubtutService{
    constructor(private http: HttpClient){}

    getSubtuts(name:string): Observable<Subtutorial[]>{
        return this.http.get<Subtutorial[]>(`api/tutorials/${name}`)
    }
}