import {SubtutService} from './subtut.service';
import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import { Subtutorial } from './subtutorial';
import { Router, ActivatedRoute} from '@angular/router'

@Component({
  selector: 'app-subtutorials',
  templateUrl: './subtut.component.html',
  styleUrls: ['./subtut.component.css'],
  providers: [SubtutService]
})
export class SubtutComponent implements OnInit {

  subtuts: Subtutorial[]

  constructor(private subtutService: SubtutService, private http: HttpClient, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    let name = this.route.snapshot.params['name'];
    this.getSubtuts(name);
    if (!name) return;
  }

  getSubtuts(name:string): void{
    this.subtutService.getSubtuts(name)
    .subscribe(subtuts => (this.subtuts = subtuts))
  }

  removeSlashes(side: string): string {
    return side.replace(/_/g, " ");
  }

}
