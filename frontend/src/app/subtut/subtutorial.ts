// Class to read Json and convert to Subtutorial object 
export class Subtutorial{
    name: string;
    path: string;
    descript: string;
    doc: string;
    author: string;
    tut_n: string;
}