import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArgsComponent } from './args.component';

describe('ArgsComponent', () => {
  let component: ArgsComponent;
  let fixture: ComponentFixture<ArgsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArgsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArgsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
