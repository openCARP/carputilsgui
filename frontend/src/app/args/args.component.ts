import {ArgsService} from './args.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Argument } from './argument';
import { Subtutorial } from '../subtut/subtutorial';
import {HttpClient} from '@angular/common/http';
import { Router, ActivatedRoute, RouterModule} from '@angular/router';
import {MatSnackBar} from '@angular/material';
import { animation } from '@angular/animations';
import { animate, state, style, transition, trigger } from '@angular/animations';
import {Observable, BehaviorSubject, Subject, Observer, of, throwError } from 'rxjs';
import { map } from 'rxjs/operators';
import {AppComponent} from '../app.component';
import { ResizeEvent } from 'angular-resizable-element';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { VisualizeComponent} from '../visualize/visualize.component';
import { HttpErrorResponse } from '@angular/common/http';
import { ScrollToBottomDirective } from './scroll-to-bottom.directive';



@Component({
  selector: 'app-args',
  templateUrl: './args.component.html',
  styleUrls: ['./args.component.css'],
  providers: [ArgsService],
  animations: [
    trigger('openCloseArgs', [
      state('open', style({display: '*', opacity: '1', visibility: 'visible'})),
      state('close', style({display: 'none', opacity: '0', visibility: 'hidden'})),
      transition('open <=> close', animate(150))
    ])
  ],
})
export class ArgsComponent implements OnInit {

  @ViewChild(ScrollToBottomDirective, {static: false}) scroll: ScrollToBottomDirective;
  public currents: boolean;
  public plotM: boolean;
  public args: Argument[];
  public subtut: Subtutorial;
  public subtutName: string;
  public showPlot: boolean;
  public argValue: number;
  public numstim: number;
  public bcl: number;
  public urlVisualize: string;
  public argId: string;
  argsOpen: string;
  public nameTut: string;
  showHelp: boolean;
  esletra: boolean;
  requiredOK: boolean;
  requiredF: boolean;
  noRequired: boolean;
  count: number;
  messageTerminal: string;
  messageTerminalAll: string;
  runFinishA: boolean;
  runFinishB: boolean;
  public style: object = {};
  labels: object [];
  groups: object [];
  groupsKeys: string [];
  groupsC: object [];
  datasets: object [];
  datasetsC: object [];
  groupsKeysC: string [];
  divs: number[];
  divsC: number[];
  traces_length: number;
  traces_lengthC: number;
  num1: number;
  visualizeWeb: boolean;
  runcarpOK: boolean;
  exp: boolean;
  novisualize: boolean;
  jsonerrror: boolean;

  constructor(private appComp: AppComponent , public snackBar: MatSnackBar, private argsService: ArgsService, private http: HttpClient,
              private route: ActivatedRoute, private router: Router) {}

  ngOnInit() {
    this.divs = [];
    this.divsC = [];
    this.traces_length = 0;
    this.traces_lengthC = 0;
    const name = this.route.snapshot.params['name'];
    this.nameTut = name;
    this.getArguments(name);
    this.getSubtut(this.nameTut);
    this.argsOpen = 'open';
    this.showHelp = false;
    this.appComp.readSubtutT('  --  ' + this.nameTut + '  --  ');
    this.requiredOK = false;
    this.noRequired = false;
    this.count = 0;
    this.messageTerminal = '';
    this.messageTerminalAll = '';
    this.runFinishA = false;
    this.runFinishB = false;
    this.visualizeWeb = false;
    this.runcarpOK = false;
    this.style = {
      height: '70%',
    };
    this.showPlot = false;
    this.currents = false;
    this.plotM = false;
    this.exp = false;
    this.novisualize = false;
    this.jsonerrror = false;
  }

  getArguments(name: string): void {
    this.argsService.getArguments(name)
    .subscribe(args => {
      this.args = args;
      this.checkNoRequired();
    });
  }

  getSubtut(nameST: string):  void {
    this.argsService.getSubtut(nameST)
    .subscribe(subtut => (this.subtut = subtut));
  }

  checkNoRequired() {
    this.count = 0;
    for (const argp of this.args) {
      if (argp.required === 'true') {
        this.count++;
      }
    }
    if (this.count === 0) {
      this.noRequired = true;
      this.requiredOK = true;
    }

  }

  argsOpenClose(): void {
    this.argsOpen = (this.argsOpen === 'open') ? 'close' : 'open';
  }

  openCloseHelp(): void {
    this.showHelp = (this.showHelp === false) ? true : false;
    if (this.showPlot) {
      this.showPlot = false;
    }
  }

  openPlot(): void {
    this.showPlot = (this.showPlot === false) ? true : false;
    if (this.showHelp) {
      this.showHelp = false;
    }
  }

  defaultV(): void {
    for (const arg of this.args) {
      arg.value = arg.default;
    }
  }

  tString(choice: string[]): boolean {
    this.esletra = typeof choice[0] === 'string';
    return this.esletra;
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message.charAt(0).toUpperCase() + message.slice(1), action, {
      duration: 4000,
    });
  }

  removeSlashes(side: string): string {
    return side.replace(/_/g, '-');
  }

  deleteFLine(side: string): string {
    return side.substring(side.indexOf('\n' , 1));
  }

  runCarpUtils(): void {
    /*
    let j: any;
    for (j in this.args) {
      if (this.args[j].value == '' || this.args[j].value == null ) {
        delete this.args[j];
      }
    }
    */
    this.initRun();
    this.argsService.runCarpUtils(this.args).subscribe(
    );
    setTimeout(() => {
      this.readRun();
    }, 1000);
    /* duration parameter for x-range 01_basic_bench and 04_limpet_fe plots */
    if (this.nameTut === '01_basic_bench') {
      for (const argp of this.args) {
        if (argp.name === 'duration') {
          this.argValue = parseFloat(argp.value);
        }
      }
    } else if (this.nameTut === '04_limpet_fe') {
        for (const argp of this.args) {
          if (argp.name === 'numstim') {
            this.numstim = parseFloat(argp.value);
          } else if (argp.name === 'bcl') {
            this.bcl = parseFloat(argp.value);
          }
        }
        this.argValue = this.numstim * this.bcl;
      }
  }

  updateTerminal(message: string) {
    this.messageTerminal += message;
  }


  readRun(): void {
    this.initRun();
    this.visualizeWeb = false;
    this.runFinishA = false;
    this.runFinishB = false;
    this.showPlot = false;
    this.plotM = false;
    this.readStream();
  }

  async readStream() {
    const decoder = new TextDecoder('utf-8');
    const res = await fetch(`/api/subtutorials/read`);
    const reader = res.body.getReader();
    while (true) {
      const { done, value } = await reader.read();
      if (done) {
        break;
      }
      this.messageTerminal += decoder.decode(value);
    }
    setTimeout(() => {
      this.responseCarpUtils();
    }, 2000);
  }


  initRun(): void {
    this.runFinishA = false;
    this.runFinishB = false;
  }

  finishRunA(): void {
    this.runFinishA = true;
  }
  finishRunB(): void {
    this.runFinishB = true;
  }


  readJson(): void {
    this.novisualize = false;
    this.jsonerrror = false;
    this.visualizeWeb = false;
    this.argsService.readJson()
    .subscribe(
      data => {
        const res = data;
        if (res['groups']) {
          this.groups = res['groups'];
          this.datasets = res['datasets'];
          this.currents = true;
          this.visualizeWeb = false;
          this.readJsonC();
        } else if (res['labels']) {
          this.visualizeWeb = false;
          this.currents = false;
          this.labels = res['labels'];
          this.datasets = res['datasets'];
        }
      },
      (error: HttpErrorResponse) => {
        this.argsService.novisualizeweb()
          .subscribe(
          data => {this.novisualize = data;
            if (!this.novisualize) {
              this.visualizeWeb = true;
            }
          });
      },
      () => {
        this.runFinishA = false;
        if (this.currents) {
          this.convertJsontoObj();
        } else {
            this.plotM = true;
            this.runFinishA = true;
        }
        }
      );
    }

  readJsonC(): void {
    this.argsService.readJsonC()
    .subscribe(
      data => {
        const res = data;
        this.groupsC = res['groups'];
        this.datasetsC = res['datasets'];
      },
      (error: HttpErrorResponse) => {
        this.jsonerrror = true;
      },
      () => {
        if (!this.visualizeWeb) {
          this.runFinishB = false;
          this.convertJsontoObjC(); }
        }
      );
  }

  readIDexp(): void {
    this.argsService.readIDexp()
    .subscribe(data => (this.argId = data));
  }

  responseCarpUtils(): void {
    this.argsService.responseCarpUtils()
    .subscribe(data => {(this.runcarpOK = data);
    if (this.runcarpOK) {
      this.readJson();
    } else {
      this.visualizeWeb = false;
      setTimeout(() => {
        this.cancelCarpUtils();
      }, 2000);
      this.messageTerminal += '\n Something went wrong......Try again \n ';
    }
    this.readIDexp();
    }
    );
  }

  convertJsontoObj(): void {
    this.groupsKeys =  Object.keys(this.groups);
    this.traces_length = this.groups[this.groupsKeys[1]]['links'].length - 1;

    for (this.num1 = 0; this.num1 < this.traces_length; this.num1++ ) {
      this.divs[this.num1] = this.num1;
      if (this.num1 === this.traces_length - 1 && !this.runFinishA) {
        this.finishRunA();
      }
    }
  }

  convertJsontoObjC(): void {
    this.groupsKeysC =  Object.keys(this.groupsC);
    this.traces_lengthC = this.groupsC[this.groupsKeysC[1]]['links'].length - 1;
    let num2 = 0;
    for (num2 = 0; num2 < this.traces_lengthC; num2++ ) {
      this.divsC[num2] = num2;
      if (num2 === this.traces_lengthC - 1 && !this.runFinishB) {
        this.finishRunB();
      }
    }
  }


  showRunning(): void {
    this.messageTerminal += ' \n  Running ... \n ';
  }


  cancelCarpUtils(): void {
    console.log('CANCELED');
    this.messageTerminal += ' \n Canceled Process \n';
    this.argsService.cancelCarpUtils().subscribe();
  }

  printArgs() {
    for (const argp of this.args) {
        console.log('Valor de:' + argp.name + ' es ' + argp.value);
    }
  }

  checkRequired() {
    if (this.noRequired === false) {
      this.requiredF = false;
      for (const argp of this.args) {
        if (argp.required === 'true') {
          if (argp.value === '') {
            this.requiredF = false;
            break;
          } else {
            this.requiredF = true;
          }
        }
      }
      if (this.requiredF) {
        this.requiredOK = true;
      } else {
        this.requiredOK = false;
      }
    }
  }

  onResizeEnd(event: ResizeEvent): void {
    this.style = {
      height: `${event.rectangle.height}px`,
    };
    window.getSelection().removeAllRanges();
  }
}






