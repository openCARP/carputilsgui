// Class to read Json and convert to Argument object 
export class Argument {
    id: number;
    name: string;
    default: string;
    required: string;
    //type : string;
    choices: string[];
    help: string;
    value: string;
    action_store: string;
    subtut_n: string;
}
