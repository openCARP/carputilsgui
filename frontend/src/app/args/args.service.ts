import {Injectable, NgZone, ViewChild} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Observable, BehaviorSubject, Observer, of, throwError } from 'rxjs';
import {flatMap, tap, map , catchError} from 'rxjs/operators';
import {Argument} from './argument';
import { Subtutorial } from '../subtut/subtutorial';
import { SseService } from './sse.service';

@Injectable({
    providedIn: 'root'
 })
export class ArgsService {
    nameSubtut: string;

    constructor(private http: HttpClient, private _zone: NgZone, private _sseService: SseService) {}

    getArguments(name: string): Observable<Argument[]>{
        return this.http.get<Argument[]>(`api/tutorial/${name}`);
    }

    getSubtut(nameST: string): Observable<Subtutorial>{
        this.nameSubtut = nameST;
        return this.http.get<Subtutorial>(`api/subtutorials/${nameST}`);
    }

    addArgument(argument: Argument): Observable<Argument>{
        return this.http.post<Argument>('api/argument', argument);
    }

    deleteArgument(name: string): Observable<{}> {
        const url = `api/arguments/${name}`;
        return this.http.delete(url);
    }

    runCarpUtils(argument: Argument[]): Observable<Argument[]> {
        console.log('RUNNING OPENCARP');
        return this.http.post<Argument[]>(`api/subtutorials/${this.nameSubtut}`, argument);
    }

    responseCarpUtils() {
        return this.http.get<boolean>(`/api/subtutorials/responseCarp`);
    }
    novisualizeweb() {
        return this.http.get<boolean>(`/api/subtutorials/novisualizeweb`);
    }

    cancelCarpUtils() {
        return this.http.post(`api/subtutorials/cancel`, 'Cancel');
    }

    readJson() {
        return this.http.get(`api/subtutorials/jsondata`);
    }

    readJsonC() {
        return this.http.get(`api/subtutorials/jsondatac`);
    }

    readIDexp() {
        return this.http.get<string>(`/api/subtutorials/idexp`);
    }
}

