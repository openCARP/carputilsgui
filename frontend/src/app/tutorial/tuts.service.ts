import {Injectable} from '@angular/core'
import {HttpClient} from '@angular/common/http'
import {Observable} from 'rxjs'
import {Tutorial} from './tutorial'
import { Subtutorial } from '../subtut/subtutorial';

@Injectable(({
    providedIn: 'root'
  }))
export class TutsService{
    constructor(private http: HttpClient){}

    getTuts(): Observable<Tutorial[]>{
        return this.http.get<Tutorial[]>('api/tutorials')
    }

    getSubtuts(name:string): Observable<Subtutorial[]>{
        return this.http.get<Subtutorial[]>(`api/tutorials/${name}`)
    }
}