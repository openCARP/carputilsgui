import {TutsService} from './tuts.service';
import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import { Tutorial } from './tutorial';
import { Subtutorial } from '../subtut/subtutorial';
import { Router, ActivatedRoute} from '@angular/router';
import {AppComponent} from '../app.component';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-tutorials',
  templateUrl: './tutorial.component.html',
  styleUrls: ['./tutorial.component.css'],
  providers:[TutsService]
})
export class TutorialComponent implements OnInit {
  tuts: Tutorial[];
  subtuts: Subtutorial[];
  nameTut: string;
  dataUpdate: boolean;


  constructor(private appComp: AppComponent, private tutsService: TutsService, private http: HttpClient,
    private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.nameTut = "";
    this.getTuts();
    this.appComp.readSubtutT(this.nameTut);
    this.dataUpdate = true;
  }

  getTuts(): void{
    this.tutsService.getTuts()
    .subscribe(tuts => (this.tuts = tuts),
                      err => console.log('HTTP Error Tutorials', err))
  }

  getSubtuts(name:string): void{
    this.tutsService.getSubtuts(name)
    .subscribe(subtuts => (this.subtuts = subtuts),
               err => console.log('HTTP Error Subtutorials', err),
               () => this.alldataUpdated());
  }


  removeSlashes(side: string): string {
    return side.replace(/_/g, " ");
  }

  alldataUpdated():void{
    this.dataUpdate = true;
    console.log('DATA UPDATED: ' + this.dataUpdate);
  }

}

