import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, Subject, BehaviorSubject} from 'rxjs';

@Injectable()

export class VisualizeService {

  constructor(private http: HttpClient) { }

  readJson() {
    return this.http.get(`api/subtutorials/jsondatapre`);

  }

  readJsonC() {
    return this.http.get(`api/subtutorials/jsondatacpre`);
  }

}
