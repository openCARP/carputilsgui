import { Component, OnInit, Input, AfterViewInit} from '@angular/core';
import { VisualizeService } from './visualize.service';
//import * as Plotly from 'plotly.js/dist/plotly.js';
//import Plotly from 'plotly.js-dist';
//import {Data, Layout} from 'plotly.js/dist/plotly.js';
import {ViewChild} from '@angular/core';
import {ElementRef} from '@angular/core';
import { strictEqual } from 'assert';
import { stringify } from 'querystring';
import { element } from 'protractor';

declare var Plotly: any;
@Component({
  selector: 'app-visualize',
  templateUrl: './visualize.component.html',
  styleUrls: ['./visualize.component.css'],
  providers: [VisualizeService]
})

export class VisualizeComponent implements OnInit, AfterViewInit {
  @Input() public plotM: boolean;
  @Input() public currents: boolean;

  @Input() public nameTut: string;
  @Input() public showPlot: boolean;
  @Input() public argValue: number;
  @Input() public divs: number[];
  @Input() public divsC: number[];
  @Input() public datasets: object;
  @Input() public groups: object;
  @Input() public datasetsC: object;
  @Input() public labels: object[];
  @Input() public groupsC: object;

  @ViewChild('container', { static: false }) container: ElementRef;
  @ViewChild('div', { static: false }) div: ElementRef;

  // Plots from Matplotlib
  layout2 = Plotly.Layout;
  layout3 = Plotly.Layout;


  duration: number;
  durationX: number [];
  groupsKeys: string [];
  datasetsKeys: string[];
  groupsKeysC: string [];
  datasetsKeysC: string[];
  num: number;
  num1: number;
  num2: number;
  totalWidthGraph: any;
  totalWidthGraph2: any;
  totalWidthGraphBefore: any;
  textRange: string[];
  layout = Plotly.Layout;
  layout1 = Plotly.Layout;
  traces_length: number;
  traces_lengthC: number;
  myDiv: any;
  myDivC: any;
  myDivM: any;
  divs_plot_traces: string[];
  divs_plot_tracesC: string[];
  plotsFinish: boolean;
  plotdat: any;
  numero: number;
  heightUpdt: number;
  widthUpdt: number;
  heightUpdt2: number;
  widthUpdt2: number;

  constructor(private visualizeService: VisualizeService) {}

  ngOnInit() {
    this.plotsFinish = false;
    this.nameTut = this.removeSlashes(this.nameTut);
    this.duration = this.argValue;
    this.durationX = [];
    this.divs_plot_traces = [];
    this.divs_plot_tracesC = [];
    this.totalWidthGraph = 0;
    this.totalWidthGraph2 = 0;
    this.myDivM = 'MyDivM';
  }

  ngAfterViewInit() {
    //const element = this.div.nativeElement;
    if (this.currents) {
      this.totalWidthGraph = this.container.nativeElement.offsetWidth;
      this.convertJsontoObj();
      this.convertJsontoObjC();
    } else if (this.plotM) {
      this.totalWidthGraph2 = this.div.nativeElement.offsetWidth;
      this.createPlotsMatplot();
    }
  }

  deleteFLine(side: string): string {
    return side.substring(side.indexOf('\n' , 1));
  }

  convertJsontoObj(): void{
    this.plotsFinish = false;
    this.groupsKeys =  Object.keys(this.groups);
    this.traces_length = this.groups[this.groupsKeys[1]]['links'].length - 1;

    let n = 0;
    while ( n < this.traces_length) {
      this.divs[n] = n;
      n += 1;
    }

    this.plotsFinish = true;
    this.datasetsKeys =  Object.keys(this.datasets);
    const linspace = require('linspace');
    this.durationX = linspace(0, this.duration, this.datasets[this.datasetsKeys[1]]['shape']['dims']);

    this.createDivsIdTraces();
  }

  convertJsontoObjC(): void{
    this.plotsFinish = false;

    // Get Groups keys JSON
    this.groupsKeysC =  Object.keys(this.groupsC);
    this.traces_lengthC = this.groupsC[this.groupsKeysC[1]]['links'].length - 1;
    let n = 0;
    while ( n < this.traces_lengthC) {
      this.divsC[n] = n;
      n += 1;
    }

    this.plotsFinish = true;
    this.datasetsKeysC =  Object.keys(this.datasetsC);
    const linspace = require('linspace');
    this.durationX = linspace(0, this.duration, this.datasets[this.datasetsKeys[1]]['shape']['dims']);

    this.createDivsIdTracesC();

  }

    // Create array divs id for html
    createDivsIdTraces() {
      for (this.num1 = 0; this.num1 < this.traces_length; this.num1++ ) {
        this.myDiv = 'MyDiv' + this.num1;
        this.divs_plot_traces.push(this.myDiv);
        if (this.num1 === this.traces_length - 1) {
          this.createPlotsTraces();
        }
      }
    }

    // Create array divsCurrent id for html
    createDivsIdTracesC() {
      for (this.num2 = 0; this.num2 < this.traces_lengthC; this.num2++ ) {
        this.myDivC = 'MyDivC' + this.num2;
        this.divs_plot_tracesC.push(this.myDivC);
        if (this.num2 === this.traces_lengthC - 1) {
          this.createPlotsTracesC();
        }
      }
    }

    // Create all trace's plots
    createPlotsTraces() {
      if (this.totalWidthGraph < 1500){
        this.heightUpdt = (this.totalWidthGraph / 3) * 0.8;
        this.widthUpdt = this.totalWidthGraph / 3;
      } else {
        this.heightUpdt = (this.totalWidthGraph / 4) * 0.8;
        this.widthUpdt = this.totalWidthGraph / 4;
      }

      let num3 = 0;
      for (num3 = 0; num3 < this.traces_length; num3++ ) {
        const trace = [{
          x: this.durationX ,
          y: this.datasets[this.datasetsKeys[num3]]['value'],
          type: 'scatter',
          marker: {color: 'red'}
        }];

        this.layout = {
          title: this.groups[this.groupsKeys[1]]['links'][num3]['title'],
          height : this.heightUpdt,
          width : this.widthUpdt,
          titlefont : {size: 12},
          showlegend: false,
          margin: {
            l: 100,
            t: 70,
            pad: 5
          },
          font : {size: 10},
          xaxis: {
            autorange: true,
            tickmode: 'auto',
            nticks: 12
          },
          yaxis: {
            autorange: true,
            tickmode: 'auto',
            nticks: 8,
            exponentformat: 'e'
          },
        };
        Plotly.react(this.divs_plot_traces[num3], trace, this.layout);
      }
    }

    // Create all current trace's plots
    createPlotsTracesC() {
      if (this.totalWidthGraph < 1500) {
        this.heightUpdt = (this.totalWidthGraph / 3) * 0.8;
        this.widthUpdt = this.totalWidthGraph / 3;
      } else {
        this.heightUpdt = (this.totalWidthGraph / 4) * 0.8;
        this.widthUpdt = this.totalWidthGraph / 4;
      }

      let num4 = 0;
      for (num4 = 0; num4 < this.traces_lengthC; num4++ ) {
        const trace = [{
          x: this.durationX ,
          y: this.datasetsC[this.datasetsKeysC[num4]]['value'],
          type: 'scatter',
          marker: {color: 'red'}
        }];

        this.layout1 = {
          title: this.groupsC[this.groupsKeysC[1]]['links'][num4]['title'],
          height : this.heightUpdt,
          width : this.widthUpdt,
          titlefont : {size: 12},
          showlegend: false,
          margin: {
            l: 100,
            t: 70,
            pad: 5
          },
          font : {size: 10},
          xaxis: {
            autorange: true,
            tickmode: 'auto',
            nticks: 12
          },
          yaxis: {
            autorange: true,
            tickmode: 'auto',
            nticks: 8,
            exponentformat: 'e'
          },
        };
        Plotly.react(this.divs_plot_tracesC[num4], trace, this.layout1);
      }
    }

    // Create plot from matplotlib tutorials
    createPlotsMatplot() {
      if (this.totalWidthGraph < 1500) {
        this.heightUpdt2 = (this.totalWidthGraph / 3);
        this.widthUpdt2 = this.totalWidthGraph / 3;
      } else {
        this.heightUpdt2 = (this.totalWidthGraph / 3);
        this.widthUpdt2 = this.totalWidthGraph / 3;
      }
      if (this.labels['labelsAB'].length > 1) {
        const trace1 = {
          x: this.datasets['valueX'],
          y: this.datasets['valueY1'],
          name: this.labels['labelsAB'][0],
          type: 'scatter',
          marker: {color: 'red'}
        };
        const trace2 = {
          x: this.datasets['valueX'],
          y: this.datasets['valueY2'],
          name: this.labels['labelsAB'][1],
          type: 'scatter',
          marker: {color: 'rgb(153, 0, 153)'}
        };

        this.layout2 = {
          height : this.heightUpdt2,
          width : this.widthUpdt2,
          showlegend: true,
          font : {size: 10},
          xaxis: {
            title: this.datasets['labelXY'][0],
            autorange: true,
            tickmode: 'auto',
            exponentformat: 'e'
          },
          yaxis: {
            title: this.datasets['labelXY'][1],
            autorange: true,
            //range: [this.datasets['ylim'][0], this.datasets['ylim'][1]],
            tickmode: 'auto',
            exponentformat: 'e'
          },
        };
        const data = [trace1, trace2];
        const config = {responsive: true};
        //console.log('Element: ' + document.getElementById('MyDivM'));
        Plotly.newPlot(this.myDivM, data, this.layout2, config);
      } else {
        const trace1 = [{
          x: this.datasets['valueX'],
          y: this.datasets['valueY1'],
          type: 'scatter',
          marker: {color: 'red'}
        }];

        this.layout3 = {
          height : this.heightUpdt2,
          width : this.widthUpdt2,
          showlegend: false,
          font : {size: 10},
          xaxis: {
            title: this.datasets['labelXY'][0],
            range: [this.datasets['xlim'][0], this.datasets['xlim'][1]],
            tickmode: 'auto',
            exponentformat: 'e'
          },
          yaxis: {
            title: this.datasets['labelXY'][1],
            range: [this.datasets['ylim'][0], this.datasets['ylim'][1]],
            tickmode: 'auto',
            exponentformat: 'e'
          },
        };
        Plotly.newPlot(this.myDivM, trace1, this.layout3);
      }
    }

  removeSlashes(side: string): string {
    side = side.replace(/_/g, ' ');
    return side[0].toUpperCase() + side.substr(1).toLowerCase();
  }
}
