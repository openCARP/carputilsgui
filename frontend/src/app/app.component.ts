import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute, Params, ChildActivationEnd, NavigationEnd, ResolveStart} from '@angular/router';
import { filter, map, take, mergeMap } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'openCARP EXAMPLES';
  opened: boolean;
  subtutTitle = '';

  readSubtutT(subtutT: string): void {
    this.subtutTitle = subtutT;
  }

  removeSlashes(side: string): string {
    return side.replace(/_/g, ' ');
  }
}
