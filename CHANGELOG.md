# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## [next release]

## [1.1] - 2020-11-09
### Added
* Experiment field is now pre-populated with a default entry
### Fixed
* x-axis in 2D plots (was showing samples instead of time)
### Changed
* Use `master` branch of experiments repository instead of GUI-specific branch
* Consider only experiments for which `GUIinclude` is set to `True` in the `run.py` file

## [1.0] - 2020-10-28
Initial public release
